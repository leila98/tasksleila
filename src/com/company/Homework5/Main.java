package com.company.Homework5;

public class Main {
    public static void main(String[] args) {


        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        String[] habits1 = {"eat", "sleep"};
        Pet pet1 = new Pet("dog", "toplan", 3, 78, habits1);
        int[][] schedule1 = new int[7][1];
        String arr1[][] = {{"Monday", "Go to school"},
                {"Tuesday", "Read a book"},
                {"Wednesday", "Buy the book"},
                {"Thursday", "Drink a coffee"},
                {"Friday", "Meet with friends"},
                {"Saturday", "Go to store"},
                {"Sunday", "Buy furniture"}};


        Human child1 = new Human("leila", "mammadova", 1998, 88, pet1, mother1, father1, arr1);


        String arr2[][] = {{"Monday", "Go to the gym"},
                {"Tuesday", "Write a code"},
                {"Wednesday", "Do delivering"},
                {"Thursday", "Do homework"},
                {"Friday", "Playing football"},
                {"Saturday", "Meet with friends"},
                {"Sunday", "Go to breakfast"}};


        Human child2 = new Human("anar", "rzayev", 2007, 43, pet1, mother1, father1, arr2);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, pet1, mother1, father1, arr2);

        Family family = new Family(mother1, father1, new Human[]{child1, child2});

        System.out.println(child1.greetPet()
                + "\n"
                +
                child1.describePet());

        family.addChild(child3);

        System.out.println(family);
        System.out.println(family.countFamily());
        family.deleteChild(child1);
        System.out.println(family);


    }
}
