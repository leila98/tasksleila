package com.company.Homework5;

import java.util.Arrays;

public class Human {
   private String name;
    private String surname;
    private int year;
    private int iq;
    private  Pet pet;
    private  Human mother;
    private  Human father;
    private String[][]schedule;
    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }
    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;

        this.mother = mother;
        this.father = father;

    }
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }



    @Override
    public String toString() {
        return "Human{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + getYear() +
                ", iq=" + getIq() +
                ", pet=" + getPet() +
                ", mother-name=" + getMother().getName() +
                ", mother-surname=" + getMother().getSurname() +
                ", father=" + getFather().getName() +
                ", father-surname=" + getFather().getSurname() +

                ", schedule=" + Arrays.deepToString(getSchedule()) +
                '}';
    }

    public Human() {



    }





    public String greetPet(){

        return "Hello, ".concat(pet.getNickname());
    }



    public String describePet(){
        String a=" ";
        if(pet.getTrickLevel()<50){
         a="very sly";

        }
        else{
            a="almost not sly";
        }
        System.out.println("I have a"+pet.getSpecies()+", he is" +pet.getAge()+ " years old, he is "+a);
        return a;
    }




}
