package com.company.Homework5;

import java.util.Arrays;

public class Family extends Human {
    public Family(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule, Human father1, Human mother1, Human[] children, Family family, Pet pet1) {
        super(name, surname, year, iq, pet, mother, father, schedule);
        this.father = father1;
        this.mother = mother1;
        this.children = children;
        this.family = family;
        this.pet = pet1;
    }

    public Family(Human mother, Human father, Human[] children, Family family) {


        this.children = children;
        this.family = family;
        this.mother = mother;
    }

    public Family(Human mother, Human father, Human[] children) {


        this.father = father;
        this.mother = mother;
        this.children = children;
    }


    @Override
    public Human getFather() {
        return father;
    }

    @Override
    public void setFather(Human father) {
        this.father = father;
    }

    @Override
    public Human getMother() {
        return mother;
    }

    @Override
    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public Pet getPet() {
        return pet;
    }

    @Override
    public void setPet(Pet pet) {
        this.pet = pet;
    }

    private Human father;

    @Override
    public String greetPet() {
      return  super.greetPet();
    }

    @Override
    public String describePet() {
      return  super.describePet();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
    private Human[] children;
    private Family family;
    private Pet pet;
    private Human mother;

    @Override
    public String toString() {
        return "Human{" +
                "father=" + getFather().getName() +
                ", mother=" + getMother().getName()+
                ", children=" + Arrays.toString(getChildren()) +
                ", family=" + getFamily() +
                ", pet=" + getPet() +
                '}';
    }



    public boolean deleteChild(Human human) {
        Human[] childrenArray = new Human[getChildren().length - 1];
        boolean delete = false;
        int index = 0;

        for (Human child : getChildren()) {
            if (!child.equals(human)) {
                childrenArray[index++] = child;
            }
        }
        this.children = childrenArray;
        if (index < childrenArray.length - 1) {
            delete = true;
        }
        return delete;
    }

    public void addChild(Human human) {
        Human[] childrenArray = new Human[children.length+1];
        for (int a = 0; a < children.length; a++) {
            childrenArray[a] = children[a];}
            childrenArray[getChildren().length] = human;
            this.children = childrenArray;
        }




    public String countFamily() {
        int members = children.length + 2;
        return "Count of members in the family:".concat(String.valueOf(members));
    }

}
